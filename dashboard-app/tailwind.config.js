/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  darkMode: 'class', // Enable dark mode
  theme: {
    extend: {
      colors: {
        primary: '#1f2937', // Darker background
        secondary: '#4b5563', // Darker cards
        accent: '#10b981', // Accent color for charts
      },
    },
  },
  plugins: [],
};
