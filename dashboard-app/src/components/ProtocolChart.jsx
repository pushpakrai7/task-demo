import React from 'react';
import { Doughnut } from 'react-chartjs-2';
import 'chart.js/auto';
import data from '../assets/eve.json';
import '../App.css'


const ProtocolChart = () => {
  const protocolCounts = data.reduce((acc, entry) => {
    const proto = entry.proto;
    acc[proto] = (acc[proto] || 0) + 1;
    return acc;
  }, {});

  const chartData = {
    labels: Object.keys(protocolCounts),
    datasets: [
      {
        data: Object.values(protocolCounts),
        backgroundColor: ['rgba(255, 205, 86, 0.5)', 'rgba(75, 192, 192, 0.5)', 'rgba(255, 159, 64, 0.5)'],
        borderColor: ['rgba(255, 205, 86, 1)', 'rgba(75, 192, 192, 1)', 'rgba(255, 159, 64, 1)'],
        borderWidth: 1,
      },
    ],
  };

  const options = {
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      legend: { labels: { color: '#ffffff' } },
    },
  };

  return (
    <div className="bg-secondary p-4 rounded-lg shadow-lg col-span-1">
      <h2 className="text-xl mb-2 text-white">Protocol Distribution</h2>
      <div className="chart-container">
        <Doughnut data={chartData} options={options} />
      </div>
    </div>
  );
};

export default ProtocolChart;
