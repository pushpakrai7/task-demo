import React from 'react';
import { Pie } from 'react-chartjs-2';
import 'chart.js/auto';
import '../App.css'
import data from '../assets/eve.json';

const SeverityChart = () => {
  const severityCounts = data.reduce((acc, entry) => {
    const severity = entry.alert ? entry.alert.severity : 0;
    acc[severity] = (acc[severity] || 0) + 1;
    return acc;
  }, {});

  const chartData = {
    labels: Object.keys(severityCounts),
    datasets: [
      {
        data: Object.values(severityCounts),
        backgroundColor: ['rgba(255, 99, 132, 0.5)', 'rgba(54, 162, 235, 0.5)', 'rgba(75, 192, 192, 0.5)'],
        borderColor: ['rgba(255, 99, 132, 1)', 'rgba(54, 162, 235, 1)', 'rgba(75, 192, 192, 1)'],
        borderWidth: 1,
      },
    ],
  };

  const options = {
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      legend: { labels: { color: '#ffffff' } },
    },
  };

  return (
    <div className="bg-secondary p-4 rounded-lg shadow-lg col-span-1">
      <h2 className="text-xl mb-2 text-white">Alert Severity Distribution</h2>
      <div className="chart-container">
        <Pie data={chartData} options={options} />
      </div>
    </div>
  );
};

export default SeverityChart;
