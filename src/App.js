import { useReducer } from "react";
import MainComponents from "./components/MainComponents";

function App() {
  const initialState = { count: 0 };

  const reducer = (state, action) => {
    switch (action.type) {
      case "increase":
        return { count: state.count + 1 };
      case "decrease":
        return { count: state.count - 1 };
      default:
        return state;
    }
  };

  const [state, dispatch] = useReducer(reducer, initialState);

  const increaseCount = () => {
    dispatch({ type: "increase" });
  };

  const decreaseCount = () => {
    dispatch({ type: "decrease" });
  };

  return (
    <>
      <div className="flex justify-center items-center min-h-screen bg-gray-500n flex-wrap gap-10 flex-col">
        <h2>Count:{state.count}</h2>
        <button
          className="btn bg-red-500 text-white border border-b-gray-600"
          onClick={increaseCount}
        >
          Increase
        </button>
        <button
          className="btn bg-red-500 text-white border border-b-gray-600"
          onClick={decreaseCount}
        >
          Decrease
        </button>

        <MainComponents />
      </div>
    </>
  );
}
export default App;
