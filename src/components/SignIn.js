import React from 'react';

const SignIn = () => {
  return (
    <div className="flex justify-center items-center min-h-screen bg-gray-50">
      <div className="bg-white p-6 sm:p-8 rounded-lg shadow-md max-w-xs w-full sm:max-w-md" style="border-radius: 48px;">
        <div className="flex justify-center mb-4">
          <img src='/image.png' alt="Qiksy Logo" className="h-12 sm:h-16" />
        </div>
        <div className="mb-4">
          <input
            type="email"
            placeholder="Email Id"
            className="w-full px-3 sm:px-4 py-2 border rounded-lg focus:outline-none focus:ring-2 focus:ring-purple-500"
          />
        </div>
        <button className="w-full py-2 bg-purple-400 text-white rounded-lg hover:bg-purple-600 transition-colors" style={{ borderRadius: '40px' }}>
          SIGN IN
        </button>
      </div>
    </div>
  );
};

export default SignIn;
