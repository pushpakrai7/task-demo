import React, { useEffect, useLayoutEffect, useRef, useState } from "react";

const MainComponents = () => {
  const [toggle, setToggle] = useState(false);

  const textRef=useRef();

  useEffect(() => {
    if (textRef.current != null) {
      const dimesions = textRef.current.getBoundingClientReact();
      console.log(dimesions);
    }
  }, [toggle]);

  return (
    <div>
      <div className="flex justify-center items-center h-[100%] gap-3 flex-wrap ">
        <h3>Name</h3>
        <button onClick={() => setToggle(!toggle)}> Toogle</button>

        {toggle && <h4 ref={textRef}> Code Layout </h4>}
      </div>
    </div>
  );
};

export default MainComponents;
